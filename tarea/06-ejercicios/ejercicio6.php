<?php

echo '<h1>Ejercicio 3</h1>';
$informacion = array(
    array(
        "departamento" => 'R.R.H.H',
        "empleado" => 'Margarita Linares',
        "salario" => '3,500'
    ),
    array(
        "departamento" => 'Contabilidad ',
        "empleado" => 'Alberto Canizales  ',
        "salario" => '2,000'
    ),
    array(
        "departamento" => 'Finanzas',
        "empleado" => 'Maria Gomez',
        "salario" => '2,500'
    ),
    array(
        "departamento" => 'Gerencia',
        "empleado" => 'Anastacia Rivera   ',
        "salario" => '3,000'
    ),
    array(
        "departamento" => 'Finanzas',
        "empleado" => 'Bladimir Romero',
        "salario" => '1,000'
    ),
    array(
        "departamento" => 'Contabilidad  ',
        "empleado" => 'Andrea Robles  ',
        "salario" => '3,000'
    ),
    array(
        "departamento" => 'R.R H.H',
        "empleado" => 'Carmen Rosales   ',
        "salario" => '1,500'
    )
);

echo '<table border="1">';
echo '<tr>';
echo '<th>Departamento</th>';
echo '<th>Empleado</th>';
echo '<th>Salario</th>';
echo '</tr>';

foreach ($informacion as $key) {
    echo '<tr>';
    foreach ($key as $value) {
        echo '<td>';
        echo $value . '<br>';
        echo '</td>';
    }

    echo '</tr>';
}
echo '</table>';
echo '<hr>';
echo '<h1>Array ordenado por nombres de los empleados:</h1>';
$nombre = array();
foreach ($informacion as $key => $empleado) {
    $nombre[$key] = $empleado["empleado"];
}
array_multisort($nombre, SORT_ASC, $informacion);
echo "<pre>";
var_dump($informacion);
echo '</pre>';
echo '<hr>';

$saldo = array(
    3500,
    2000,
    2500,
    3000,
    1000,
    3000,
    1500,
);
echo '<h1>El promedio del salario de los empleados</h1>';
for($i = 0; $i< count($saldo); $i++){
    $suma += $saldo[$i];
}
echo "La suma de los salarios son: $suma <br>";
$promedio += $suma / count($saldo);
echo "El promedio de los salarios es: $promedio";
