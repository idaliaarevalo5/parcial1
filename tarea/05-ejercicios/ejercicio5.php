<?php
echo '<hr>';
echo '<h1>Ejercicio 2</h1>';

$datos = array(
    array(
        "nombre" => 'Francisco Garcia',
        "Telefono" => '5689741523',
        "Correo" => 'Fgarcia@gmail.com'
    ),
    array(
        "nombre" => 'Manual Salinas',
        "Telefono" => '2584369721',
        "Correo" => 'Msalinas@gmail.com'
    ),
    array(
        "nombre" => 'Johana melara',
        "Telefono" => '9875147536',
        "Correo" => 'Jmelara@gmail.com'
    )
);

echo '<table border="1">';
echo '<tr>';
echo '<th>Nombre</th>';
echo '<th>Telefono</th>';
echo '<th>Correo</th>';
echo '</tr>';

foreach ($datos as $key) {
    echo '<tr>';
    foreach ($key as $value) {
        echo '<td>';
        echo $value . '<br>';
        echo '</td>';
    }

    echo '</tr>';
}
echo '</table>';
echo '<br>';
echo '<hr>';
