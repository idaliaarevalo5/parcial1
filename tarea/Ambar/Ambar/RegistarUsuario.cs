﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
namespace Ambar
{
    public partial class RegistarUsuario : Form
    {
        public RegistarUsuario()
        {
            InitializeComponent();
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {


            DialogResult respuesta;
            if (txtNombreC.Text == "")
            {
                MessageBox.Show("Debe escribir el nombre del usuario");
                txtNombreC.Focus();
            }
            else if (txtNombreC.Text == "")
            {
                MessageBox.Show("Debe escribir una clave");
                txtClave1.Focus();

            }

            respuesta = MessageBox.Show("Esta seguro que desea registrar al usuario?", "Registro de Usuario", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (respuesta == DialogResult.Yes)
            {
                try

                {
                    //Esta es mi cadena de conexión. He asignado la ruta de la dirección del archivo de la base de datos.
                    string MyConnection2 = ("server = 127.0.0.1; database =mydb; Uid = root; pwd =usbw; port=3307;");
                    //Esta es mi consulta de inserción en la que estoy tomando información del usuario a través de formularios de Windows 
                    string Query = "insert into tusuario(Nombre,Contrasena) values('" + this.txtNombreC.Text + "','" + this.txtClave1.Text + "');";
                    // Esta es MySqlConnection aquí. He creado el objeto y paso mi cadena de conexión.
                    MySqlConnection MyConn2 = new MySqlConnection(MyConnection2);

                    //Esta es la clase de comando que manejará la consulta y el objeto de conexión. 

                    MySqlCommand MyCommand2 = new MySqlCommand(Query, MyConn2);
                    MySqlDataReader MyReader2;

                    MyConn2.Open();
                    MyReader2 = MyCommand2.ExecuteReader();     //Aquí nuestra consulta será ejecutada y los datos guardados en la base de datos.
                    MessageBox.Show("El Registro Se Guardo Exitosamente");
                    while (MyReader2.Read())

                    {
                    }

                    MyConn2.Close();
                }

                catch (Exception ex)

                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                Close();
            }
            try
            {
                string MyConnection2 = ("server = 127.0.0.1; database = mydb; Uid = root; pwd =usbw; port=3307; ");

                string Query = "select * from tusuario";
                MySqlConnection MyConn2 = new MySqlConnection(MyConnection2);
                MySqlCommand MyCommand2 = new MySqlCommand(Query, MyConn2);

                MySqlDataAdapter MyAdapter = new MySqlDataAdapter();
                MyAdapter.SelectCommand = MyCommand2;
                DataTable dTable = new DataTable();
                MyAdapter.Fill(dTable);
                DataGridView1.DataSource = dTable;
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }

        private void RegistarUsuario_Load(object sender, EventArgs e)
        {
            try
            {
                string MyConnection2 = ("server = 127.0.0.1; database = mydb; Uid = root; pwd =usbw; port=3307");

                string Query = "select * from tusuario";
                MySqlConnection MyConn2 = new MySqlConnection(MyConnection2);
                MySqlCommand MyCommand2 = new MySqlCommand(Query, MyConn2);

                MySqlDataAdapter MyAdapter = new MySqlDataAdapter();
                MyAdapter.SelectCommand = MyCommand2;
                DataTable dTable = new DataTable();
                MyAdapter.Fill(dTable);
                DataGridView1.DataSource = dTable;
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            DialogResult respu;
            respu = MessageBox.Show("Esta seguro que desea eliminar el usuario?", "Registro de Usuario", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (respu == DialogResult.Yes)
            {
                try
                {
                    string MyConnection2 = ("server = 127.0.0.1; database = mydb; Uid = root; pwd =usbw; port=3307; ");
                    string Query = "delete from tusuario where Nombre='" + this.txtNombreC.Text + "';";
                    MySqlConnection MyConn2 = new MySqlConnection(MyConnection2);
                    MySqlCommand MyCommand2 = new MySqlCommand(Query, MyConn2);
                    MySqlDataReader MyReader2;
                    MyConn2.Open();
                    MyReader2 = MyCommand2.ExecuteReader();
                    MessageBox.Show("Registro Eliminado");
                    while (MyReader2.Read())
                    {
                    }
                    MyConn2.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                try
                {
                    string MyConnection2 = ("server = 127.0.0.1; database = mydb; Uid = root; pwd =usbw; port=3307; ");

                    string Query = "select * from tusuario";
                    MySqlConnection MyConn2 = new MySqlConnection(MyConnection2);
                    MySqlCommand MyCommand2 = new MySqlCommand(Query, MyConn2);

                    MySqlDataAdapter MyAdapter = new MySqlDataAdapter();
                    MyAdapter.SelectCommand = MyCommand2;
                    DataTable dTable = new DataTable();
                    MyAdapter.Fill(dTable);
                    DataGridView1.DataSource = dTable;
                }

                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);

                }
                txtClave1.Text = "";
                txtNombreC.Text = "";
            }
        }

        private void DataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

            txtNombreC.Text = DataGridView1.Rows[e.RowIndex].Cells["Nombre"].Value.ToString();
            txtClave1.Text = DataGridView1.Rows[e.RowIndex].Cells["Contrasena"].Value.ToString();

        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void txtNombreC_TextChanged(object sender, EventArgs e)
        {

        }

        private void DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
