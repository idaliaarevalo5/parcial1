﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
namespace Ambar
{
    public partial class Parquesito : Form
    {
        public Parquesito()
        {
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            DialogResult respuesta;
            if (txtClave1.Text == "")
            {
                MessageBox.Show("Debe escribir hora de entrada");
                txtClave1.Focus();
            }
            else if (txtclave2.Text == "")
            {
                MessageBox.Show("Debe escribir numero de parqueo");
                txtclave2.Focus();

            }
            else if (dtpfecha.Text == "")
            {
                MessageBox.Show("Debe escribir numero de parqueo");
                dtpfecha.Focus();

            }
            else if (txtPlaca.Text == "")
            {
                MessageBox.Show("Debe de escribir el numero de placa");
                txtPlaca.Focus();
            }
          

            respuesta = MessageBox.Show("Esta seguro que desea registrar?", "Registro de entrada", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (respuesta == DialogResult.Yes)
            {
                try

                {
                    //Esta es mi cadena de conexión. He asignado la ruta de la dirección del archivo de la base de datos.
                    string MyConnection2 = ("server = localhost; database =mydb; Uid = root; pwd =usbw; port=3307; ");
                    //Esta es mi consulta de inserción en la que estoy tomando información del usuario a través de formularios de Windows 
                    string Query = "insert into tparqueo(Placa,Precio,Entrada,Parqueo,Tiempo) values('" + this.txtPlaca .Text + "','" + this.txtPrecio.Text  + "','" + this.txtClave1.Text + "','" + this.txtclave2.Text + "','" + this.dtpfecha.Text + "');";
                    // Esta es MySqlConnection aquí. He creado el objeto y paso mi cadena de conexión.
                    MySqlConnection MyConn2 = new MySqlConnection(MyConnection2);

                    //Esta es la clase de comando que manejará la consulta y el objeto de conexión. 

                    MySqlCommand MyCommand2 = new MySqlCommand(Query, MyConn2);
                    MySqlDataReader MyReader2;

                    MyConn2.Open();
                    MyReader2 = MyCommand2.ExecuteReader();     //Aquí nuestra consulta será ejecutada y los datos guardados en la base de datos.
                    MessageBox.Show("El Registro Se Guardo Exitosamente");
                    while (MyReader2.Read())

                    {
                    }

                    MyConn2.Close();
                }

                catch (Exception ex)

                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                Close();
            }

            try
            {
                string MyConnection2 = ("server = localhost; database = mydb; Uid = root; pwd =usbw; port=3307; ");

                string Query = "select * from tparqueo";
                MySqlConnection MyConn2 = new MySqlConnection(MyConnection2);
                MySqlCommand MyCommand2 = new MySqlCommand(Query, MyConn2);

                MySqlDataAdapter MyAdapter = new MySqlDataAdapter();
                MyAdapter.SelectCommand = MyCommand2;
                DataTable dTable = new DataTable();
                MyAdapter.Fill(dTable);
                DTG1.DataSource = dTable;
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult respu;
            respu = MessageBox.Show("Esta seguro que desea eliminar ?", "Registro ", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (respu == DialogResult.Yes)
            {
                try
                {
                    string MyConnection2 = ("server = 127.0.0.1; database = mydb; Uid = root; pwd =usbw; port=3307; ");
                    string Query = "delete from tparqueo where Entrada='" + this.txtClave1.Text + "';";
                    MySqlConnection MyConn2 = new MySqlConnection(MyConnection2);
                    MySqlCommand MyCommand2 = new MySqlCommand(Query, MyConn2);
                    MySqlDataReader MyReader2;
                    MyConn2.Open();
                    MyReader2 = MyCommand2.ExecuteReader();
                    MessageBox.Show("Registro Eliminado");
                    while (MyReader2.Read())
                    {
                    }
                    MyConn2.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                try
                {
                    string MyConnection2 = ("server = localhost; database = mydb; Uid = root; pwd =usbw; port=3307; ");

                    string Query = "select * from tparqueo";
                    MySqlConnection MyConn2 = new MySqlConnection(MyConnection2);
                    MySqlCommand MyCommand2 = new MySqlCommand(Query, MyConn2);

                    MySqlDataAdapter MyAdapter = new MySqlDataAdapter();
                    MyAdapter.SelectCommand = MyCommand2;
                    DataTable dTable = new DataTable();
                    MyAdapter.Fill(dTable);
                    DTG1.DataSource = dTable;
                }

                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);

                }
              
                txtPlaca.Text = "";
                txtClave1.Text = "";
                txtclave2.Text = "";
                dtpfecha.Text = "";
            }
        }

        private void Parquesito_Load(object sender, EventArgs e)
        {
            try
            {
                string MyConnection2 = ("server = localhost; database = mydb; Uid = root; pwd =usbw; port=3307; ");

                string Query = "select * from tparqueo";
                MySqlConnection MyConn2 = new MySqlConnection(MyConnection2);
                MySqlCommand MyCommand2 = new MySqlCommand(Query, MyConn2);

                MySqlDataAdapter MyAdapter = new MySqlDataAdapter();
                MyAdapter.SelectCommand = MyCommand2;
                DataTable dTable = new DataTable();
                MyAdapter.Fill(dTable);
                DTG1.DataSource = dTable;
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }

        private void DTG1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            txtPlaca .Text = DTG1.Rows[e.RowIndex].Cells["Placa"].Value.ToString();
            txtPrecio.Text = DTG1.Rows[e.RowIndex].Cells["Precio"].Value.ToString();
            txtClave1.Text = DTG1.Rows[e.RowIndex].Cells["Entrada"].Value.ToString();
            txtclave2.Text = DTG1.Rows[e.RowIndex].Cells["Parqueo"].Value.ToString();
            dtpfecha.Text = DTG1.Rows[e.RowIndex].Cells["Tiempo"].Value.ToString();
        }

        private void DTG1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void txtPrecio_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
